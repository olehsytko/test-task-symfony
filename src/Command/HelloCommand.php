<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'HelloCommand',
    description: 'Add a short description for your command',
)]
class HelloCommand extends Command
{
    protected function configure(): void
    {
        $this->addArgument('variable', InputArgument::OPTIONAL, 'Argument description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('variable');

        while (true) {
            $io->writeln(sprintf('Hello %s', $arg1));
            sleep(1);
        }

        return Command::SUCCESS;
    }
}
