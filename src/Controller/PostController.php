<?php

namespace App\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    #[Route('/post', name: 'homepage')]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Post::class);

        $posts = $repository->findAll();

        return $this->render('post/index.html.twig', [
            'posts' => $posts,
        ]);
    }

    #[Route('/post/create', name: 'create_post', methods: ['POST'])]
    public function create(Request $request, EntityManagerInterface $entityManager): Response
    {
        $post = new Post();
        $post->setText($request->get('text'));

        $entityManager->persist($post);

        $entityManager->flush();

        return $this->redirectToRoute('homepage');
    }

    #[Route('/post/show/{id}', name: 'show_post', methods: ['GET', 'HEAD'])]
    public function show(EntityManagerInterface $entityManager, int $id): Response
    {
        $post = $entityManager->getRepository(Post::class)->find($id);

        return $this->render('post/show.html.twig', [
            'post' => $post,
        ]);
    }

    #[Route('/post/update/{id}',  name: 'update_post', methods: ['POST'])]
    public function update(Request $request, EntityManagerInterface $entityManager, int $id): Response
    {
        $post = $entityManager->getRepository(Post::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException(
                'No post found for id '. $id
            );
        }

        $post->setText($request->get('text'));

        $entityManager->flush();

        return $this->redirectToRoute('show_post', ['id' => $post->getId()]);
    }

    #[Route('/post/delete/{id}', name: 'delete_post', methods: ['POST'])]
    public function delete(EntityManagerInterface $entityManager, int $id): Response
    {
        $post = $entityManager->getRepository(Post::class)->find($id);

        $entityManager->remove($post);
        $entityManager->flush();

        return $this->redirectToRoute('homepage');
    }
}
